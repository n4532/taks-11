#include "Number.h"

Number::Number()
{
}

Number::Number(int num)
{
	this->number = num;
	this->repetitions = 1;
}

void Number::addRepetition()
{
	this->repetitions++;
}

void Number::addRepetition(int n)
{
	this->repetitions += n;
}

void Number::setNumber(int n)
{
	this->number = n;
}

void Number::setRepetitions(int n)
{
	this->repetitions = n;
}

int Number::getNumber()
{
	return this->number;
}

int Number::getRepetitions()
{
	return this->repetitions;
}
