#pragma once
class Number
{
public: 
	Number();
	Number(int);
	void addRepetition();
	void addRepetition(int);
	void setNumber(int);
	void setRepetitions(int);
	int getNumber();
	int getRepetitions();
private:
	int number;
	int repetitions = 0;
};


