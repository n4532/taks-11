#define _CRT_SECURE_NO_WARNINGS
#include <iostream>
#include <fstream>
#include "Number.h"

using namespace std;

void createTextFile(char*, char*, char*);
void displayTextFile(char*);
void convertToBinaryFile(char*, char*);
void displayBinaryFile(char*);

int main() {
	const int N = 256;
	char firstName[N] = "text1.txt";
	char secondName[N] = "text2.txt";
	char fileName[N] = "text3.txt";
	char binary[N] = "list.bin";

	createTextFile(firstName, secondName, fileName);
	displayTextFile(fileName);

	convertToBinaryFile(fileName, binary);
	displayBinaryFile(binary);
}

void createTextFile(char* firstName, char* secondName, char* finalName)
{
	ifstream first;
	ifstream second;
	ofstream file;

	first.open(firstName);
	second.open(secondName);
	file.open(finalName);

	if (!first.is_open() || !second.is_open() || !file.is_open())
	{
		cout << "\nCan't open file to write!";
	}

	int m = 0;
	int n = 0;

	first >> m;
	second >> n;

	while (!second.eof() && !first.eof()) {
		if (n < m) {
			file << n << " ";
			second >> n;
		}
		else {
			file << m << " ";
			first >> m;
		}
	}

	if (first.eof()) {
		while (!second.eof()) {
			second >> n;
			file << n << " ";
		}
	}
	else {
		while (!first.eof()) {
			first >> m;
			file << m << " ";
		}
	}

	first.close();
	second.close();
	file.close();
}

void displayTextFile(char* name)
{
	ifstream fin;
	fin.open("text3.txt");

	while (!fin.eof()) {
		int n = 0;
		fin >> n;
		if (n != 0)
			cout << n << " ";
	}

	fin.close();
}

void convertToBinaryFile(char* fileName, char* binaryName) {
	ifstream finally;
	finally.open(fileName);
	ofstream fbin;
	fbin.open(binaryName, ios::binary);

	if (!finally.is_open() || !fbin.is_open())
	{
		cout << "\nCan't open file to write!";
	}

	int element;
	finally >> element;

	Number* num = new Number;
	int bufSize = sizeof(Number);
	num->setNumber(element);
	num->setRepetitions(1);

	while (!finally.eof()) {
		finally >> element;

		if (num->getNumber() == element) {
			num->addRepetition();
		}
		else {
			Number* binaryNumber = new Number;
			binaryNumber->setNumber(num->getNumber());
			binaryNumber->setRepetitions(num->getRepetitions());
			fbin.write((char*)&binaryNumber, bufSize);

			num->setRepetitions(1);
			num->setNumber(element);
		}
	}

	num->setRepetitions(num->getRepetitions() - 1);
	fbin.write((char*)&num, bufSize);

	finally.close();
	fbin.close();
}

void displayBinaryFile(char* fileName)
{
	ifstream streamIn(fileName, ios::binary);
	if (!streamIn.is_open())
	{
		cout << "Can't open file to read!";
		return;
	}
	int bufSize = sizeof(Number);
	Number* number = new Number;
	cout << endl;
	while (streamIn.read((char*)&number, bufSize))
	{
		cout << number->getNumber() << "---" << number->getRepetitions() << endl;
	}
	streamIn.close();
}